## Interrupt driven UART I/O module

This module implements UART transmit and receive FIFOs.

### Usage

- Call rx_init() after UART_Init to enable the receive interrupt.
- Replace calls to UART_TxString(text) with txstring(text)
- Replace calls to UART_RxString(rx) with the code below:

``` c++
int n;
n = rxstring(rx, sizeof(rx)-1);
rx[n] = '\0';
```
