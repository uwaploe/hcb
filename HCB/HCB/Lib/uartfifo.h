#ifndef _UARTFIFO_H_
#define _UARTFIFO_H_ 1

void rx_init(void);
char rxchar(void);
int rxready(void);
int rxstring(char *p, int n);
int rxerrors(void);
void txchar(char c);
int txstring_noblock(char *p);

#endif
