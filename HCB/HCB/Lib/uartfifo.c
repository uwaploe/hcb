// Interrupt driven UART transmit/receive module for AVR
#include <util/atomic.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "uartfifo.h"


// Maximum capacity + 1
#define FIFOSIZE        128

typedef struct {
    int head;
    int tail;
    int overruns;
    char buf[FIFOSIZE];
} Fifo_t;

static Fifo_t rxFifo = {.head = 0, .tail = 0, .overruns = 0};
static Fifo_t txFifo = {.head = 0, .tail = 0, .overruns = 0};

// Return true if the FIFO is empty
static int fifo_is_empty(Fifo_t *f)
{
    return f->head == f->tail;
}

// Return the current FIFO size
static int fifo_size(Fifo_t *f)
{
    int n = f->tail - f->head;
    return n < 0 ? (n + FIFOSIZE) : n;
}

// Return true if the FIFO is full
static int fifo_is_full(Fifo_t *f)
{
    return fifo_size(f) == (FIFOSIZE-1);
}

// Add c to the tail of the FIFO
static void fifo_push(Fifo_t *f, char c)
{
    f->buf[f->tail++] = c;
    f->tail = f->tail % FIFOSIZE;
}

// Remove character from head of the FIFO
static char fifo_pop(Fifo_t *f)
{
    char c;
    c = f->buf[f->head++];
    f->head = f->head % FIFOSIZE;
    return c;
}

// Enable the receive interrupt
void rx_init(void)
{
    UCSRB |= (1 << RXCIE);
}

// Receive a single character from the input FIFO.
char rxchar(void)
{
    char c;

    while(fifo_is_empty(&rxFifo))
        ;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        c = fifo_pop(&rxFifo);
    }

    return c;
}

// Return true if at least one character can be read from the input FIFO
int rxready(void)
{
    return !fifo_is_empty(&rxFifo);
}

// Read at most n characters from the input FIFO stopping at the first
// carriage-return or line-feed character. All read characters are echoed to
// the output FIFO. The return value is the number of characters read, the
// string will only be null-terminated if a CR or LF is read from the input.
int rxstring(char *p, int n)
{
    char c;
    int len = 0;

    while(len < n)
    {
        c = rxchar();
        txchar(c);
        if(c == '\r' || c == '\n')
        {
            p[len] = '\0';
            break;
        }

        if(c == '\b')
        {
            if(len > 0)
                len--;
            continue;
        }

        p[len++] = c;
    }

    return len;
}
// Return the count of input FIFO over-run errors
int rxerrors(void)
{
    return rxFifo.overruns;
}

// Write a single character to the output FIFO
void txchar(char c)
{
    // Block while the output FIFO is full
    while(fifo_is_full(&txFifo))
        ;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        fifo_push(&txFifo, c);
    }
    // enable DRE interrupt
    UCSRB |= (1<<UDRIE);
}

// Return true if at least one character can be written to the output FIFO
int txready(void)
{
    return !fifo_is_full(&txFifo);
}

// Write a null-terminated string to the output FIFO
void txstring(char *p)
{
    while(*p)
        txchar(*p++);
}

// Write a null-terminated string to the output FIFO without blocking. Returns
// the number of characters written.
int txstring_noblock(char *p)
{
    int n = 0;
    while(*p)
    {
        if(fifo_is_full(&txFifo))
            break;
        txchar(*p++);
        n++;
    }

    return n;
}

ISR(USART_RXC_vect)
{
    char c;

    if(!fifo_is_full(&rxFifo))
    {
        // read from uart
        c = UDR;
        fifo_push(&rxFifo, c);
    }
    else
        rxFifo.overruns++;
}

ISR(USART_UDRE_vect)
{
    char c;
    if(!fifo_is_empty(&txFifo))
    {
        c = fifo_pop(&txFifo);
        // write to uart
        UDR = c;
    }
    else
    {
        // disable DRE interrupt
        UCSRB &= ~(1<<UDRIE);
    }
}
