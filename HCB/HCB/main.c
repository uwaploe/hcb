/*
 * HCB.c
 *
 * Created: 1/13/2021 5:25:53 PM
 * Author : Lab 320
 */ 
#include "adc.h"
#include "uart.h"
#include "i2c.h"
#include "delay.h"
#include <avr/interrupt.h>
#include <stdbool.h>
//#define DEBUG /* uncomment to enable debug information via UART */


// bit manipulation macros.
#define sbi(var, mask)   ((var) |= (uint8_t)(1 << mask))
#define cbi(var, mask)   ((var) &= (uint8_t)~(1 << mask))


#define Fan1Address 0x00
#define controlBit 0x50
#define read 1
#define write 0
#define BUFFER_SIZE 100			// size of Rx ring buffer.

/*
 Function Prototypes:
*/
ISR(INT0_vect);

/*
 Global Variables:
 Variables appearing in both ISR/Main are defined as 'volatile'.
*/
volatile int rxn=0,pin_status; // buffer 'element' counter.
volatile char rx[BUFFER_SIZE]; // buffer of 'char'.
volatile uint8_t rxFlag = 0; //entire byte wasted for using a single bit
bool isheat = false;
bool cooling = false;
volatile char ch,rx_flag =0,i =0;

int hot = 80;
int hyst = 2;

ISR(INT0_vect)
{
	 pin_status = PIND & (1<<2);
	 if(pin_status)		
	 {
		 isheat = true;
		 PORTD = PORTD | (1<<3);	
		 UART_TxString("$HEATING\n\r");
		 setFan1(0x1F);
		 heatloop();
	 }
	 else
	 {
		 isheat = false;
		 PORTD = PORTD & (~(1<<3));	
		 UART_TxString("$COOLING\n\r");
	 }
	DELAY_ms(50);  	/* Software debouncing control delay */
	
}

int main()
{
	int adcValue;
	
	ADC_Init();       /* Initialize the ADC module */
	UART_Init(9600);  /* Initialize UART at 9600 baud rate */
	I2C_Init();
	initGPIO();
	
	 
	
	while(1)
	{
		

			if(UART_RxChar() == '$')
			{
				UART_RxString(rx);
				//UART_TxString(rx);
				if (strcmp("ADC",rx) == 0)
				{
					getADC();
				
				}
				if(strcmp("FAN,OFF",rx) == 0)
				{
					setFan1(0x00);
					DELAY_ms(100);
					readFan1();
				}
				if(strcmp("FAN,LOW",rx) == 0)
				{
					setFan1(0x0A);
					DELAY_ms(100);
					readFan1();
				}
				if(strcmp("FAN,MED",rx) == 0)
				{
					setFan1(0x0F);
					DELAY_ms(100);
					readFan1();
				}
				if(strcmp("FAN,HIGH",rx) == 0)
				{
					setFan1(0x1F);
					DELAY_ms(100);
					readFan1();
				}
				/*
				if(strcmp("SETFAN",rx) ==0)
				{
					UART_TxString("Setting Fan Speed\n\r");
					while(UART_RxChar() == '$')
					{
						UART_TxString("here\n\r");
						UART_RxString(rx);
						setFan1(rx[0]<<8|rx[1]);
						DELAY_ms(100);
						readFan1();
					}
				}
				*/
			}
		
	}
	
	return (0);
}

void heatloop()
{
	while(isheat)
	{
		 pin_status = PIND & (1<<2);
		 if(pin_status)
		 {
			 if(ADC_GetAdcValue(0) < hot && !cooling)
			 {
				 PORTD = PORTD | (1<<3);
				 cooling = false;
			 }
			 else if( ADC_GetAdcValue(0) > (hot +hyst))
			 {
				 cooling = true;
				  PORTD = PORTD & (~(1<<3));
				  UART_TxString("$COOLING\n\r");
			 }
			 if( ADC_GetAdcValue(0)< (hot-hyst) && cooling)
			 {
				 cooling = false;
			 }
			 
		 }
		 else
		 {
			 isheat = false;
			 PORTD = PORTD & (~(1<<3));
			 UART_TxString("$COOLING\n\r");
		 }
		getADC();
		DELAY_us(10);
		
	}
}


void setFan1(uint8_t set)
{
	int fanPWM;
	I2C_Start();
	I2C_Write(controlBit | Fan1Address | write);
	I2C_Write(set);
	I2C_Stop();
	
}

void readFan1()
{
	uint8_t fanPWM;
	I2C_Start();
	I2C_Write(controlBit | Fan1Address | read);
	//I2C_Write(0x00);
	fanPWM = I2C_Read(0x00);
	I2C_Stop();
	
	UART_Printf("I2C:,%4d",fanPWM);
}

void getADC()
{
	int adcValue;
	UART_TxString("$ADCH");
	for(int j = 0; j <8; j++)
	{
		adcValue = ADC_GetAdcValue(j);
		UART_Printf(",%4d",adcValue);
	}
	UART_TxString("*00\n\r");
	
	
}

void initGPIO()
{
	
	DDRD = DDRD | (1<<3);	//EN_SWITCH OUTPUT
    DDRD = DDRD & (~(1<<2));		/* Make PD2 as input pin */
    PORTD = PORTD | (1<<2);
	
	GICR = 1<<INT0;		// Enable INT0
	MCUCR = 0<<ISC01 | 1<<ISC00;  /* Trigger INT1 on rising edge */
	
	sei();
}

